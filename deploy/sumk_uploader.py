from excel_generator.components.excel_generator import sumk_generation
from excel_generator.components.sumk_config import EnvironmentConfigProvider
from excel_generator.components.sumk_worksheets import SUMKParams
from excel_generator.components.sumk_uploader import SumkUploader
from pydantic_yaml import YamlModel
import pathlib as pl


class SumkYamlModel(YamlModel):
    excel_model: SUMKParams = None


def main():
    sumk_config_filename = pl.Path(__file__). \
        parent.absolute().joinpath('excel_config.yml')
    sumk_yaml_model = SumkYamlModel.parse_file(sumk_config_filename, content_type="yaml")
    excel_filename = sumk_generation(sumk_yaml_model.excel_model)
    env_config = EnvironmentConfigProvider()
    sumk_uploader = SumkUploader(env_config.get_ftp_host(),
                                 env_config.get_ftp_login(),
                                 env_config.get_ftp_location(),
                                 env_config.get_ftp_password())
    sumk_uploader.upload(excel_filename)


if __name__ == '__main__':
    main()
