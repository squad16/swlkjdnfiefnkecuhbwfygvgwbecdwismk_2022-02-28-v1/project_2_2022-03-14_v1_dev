from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='project_2_2022-03-14_v1_dev',
    packages=find_packages(where='prod', exclude=['tests']),
    package_dir={'': 'prod'},
    version=version,
    description='project 2',
    author='po_1 po_1'
)
